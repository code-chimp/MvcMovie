const webpack = require('webpack')
const { resolve } = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = env => {
  return {
    entry: {
      app: './js/index.js',
      vendor: ['lodash', 'jquery']
    },
    output: {
      filename: 'bundle.[name].[chunkhash].js',
      path: resolve(__dirname, 'wwwroot', 'js'),
      pathinfo: !env.prod
    },
    context: resolve(__dirname, 'src'),
    devtool: env.prod ? 'source-map' : 'eval',
    bail: env.prod,
    module: {
      loaders: [{
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      }, {
        test: /\.css$/,
        loader: 'style!css'
      }, {
        test: /\.less$/,
        loader: 'style!css!less'
      }, {
        test: /\.(sass|scss)$/,
        loader: 'style!css!sass'
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.(otf|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader'
      }]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './index.html'
      }),
      new HtmlWebpackPlugin({
        template: './_Layout.cshtml'
      }),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor'
      })
    ]
  }
}

import webpack from 'webpack'
import path from 'path'
const webroot = path.join(__dirname, 'wwwroot')

const config = {
  devtool: 'cheap-module-eval-source-map',
  debug: true,
  noInfo: false,

  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './js_src/index.js'
  ],

  target: 'web',
  output: {
    path: path.join(webroot, 'js'),
    publicPath: '/',
    filename: 'medefis.js'
  },

  resolve: {
    extensions: ['', '.js']
  },

  module: {
    loaders: [
      {
        test: /\.js?$/,
        include: [
          path.resolve(__dirname, 'js_src')
        ],
        loader: 'react-hot!babel'
      }, {
        test: /\.css/,
        loader: 'style!css'
      }, {
        test: /\.less$/,
        loader: 'style!css!less'
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.(otf|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader'
      }
    ]
  },

  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
    hot: true
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
}

export default config

import React from 'react'
import { render } from 'react-dom'
import {
  browserHistory,
  Router
} from 'react-router'

import '../../node_modules/toastr/build/toastr.min.css'
import './styles/styles.scss'

import routes from './routes'

const updateScroll = () => {
  window.scrollTo(0, 0)
}

render(
  <Router
    history={browserHistory}
    routes={routes}
    onUpdate={updateScroll} />,
  document.getElementById('medefis-app')
)

import React, { PropTypes } from 'react'

class Main extends React.Component {
  // constructor (props, context) {
  //   super(props, context)
  // }

  render () {
    return (
      <div className='container-fluid'>
        {this.props.children}
      </div>
    )
  }
}

Main.propTypes = {
  children: PropTypes.node.isRequired
}

export default Main

import React from 'react'

const FourOhFour = () => {
  return (
    <div className='four-oh-four'>
      <h2>Dude, where's my page</h2>
    </div>
  )
}

export default FourOhFour

import {
  INITIAL_STATE,
  BEGIN_AJAX_CALL,
  AJAX_CALL_ERROR
} from '../constants'

function actionTypeEndsInSuccess (type) {
  return type.substring(type.length - 8) === '_SUCCESS'
}

export default function ajaxStatusReducer (state = INITIAL_STATE.ajaxCallsInProgress, action) {
  if (action.type === BEGIN_AJAX_CALL) {
    return state + 1
  } else if (action.type === AJAX_CALL_ERROR) {
    return state - 1
  } else if (actionTypeEndsInSuccess(action.type)) {
    return state - 1
  }

  return state
}

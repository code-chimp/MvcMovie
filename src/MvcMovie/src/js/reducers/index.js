import { combineReducers } from 'redux'

import ajaxCallsInProgress from './ajax_status_reducer'

const rootReducer = combineReducers({
  ajaxCallsInProgress
})

export default rootReducer

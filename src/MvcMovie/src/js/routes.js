import React from 'react'
import {
  IndexRoute,
  Route
} from 'react-router'

import Main from './components/main'
import Home from './components/home'
import FourOhFour from './components/four_oh_four'

const routes = (
  <Route path='/' component={Main}>
    <IndexRoute component={Home} />
    <Route path='*' component={FourOhFour} />
  </Route>
)

export default routes

import { expect } from 'chai'
import React from 'react'
import {
  renderIntoDocument,
  scryRenderedDOMComponentsWithTag
} from 'react-addons-test-utils'

import Home from '../../js_src/components/home'

describe('Home', () => {
  it('renders', () => {
    const component = renderIntoDocument(
      <Home />
    )
    const headings = scryRenderedDOMComponentsWithTag(component, 'h1')

    expect(headings.length).to.equal(1)
    expect(headings[0].textContent).to.equal('Home')
  })
})

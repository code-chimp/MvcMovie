var gulp = require('gulp')
var del = require('del')
var config = require('../config')

var cleanTask = function (callback) {
  var basePath = config.root.dest
  var cleanPaths = [
    basePath + '/js/**',
    '!' + basePath + '/js',
    basePath + '/images/**',
    '!' + basePath + '/images',
    basePath + '/css/**',
    '!' + basePath + '/css',
    basePath + '/fonts/**',
    '!' + basePath + '/fonts',
    basePath + '/lib/**',
    '!' + basePath + '/lib',
    basePath + '/*.{png,ico,json,xml,js,txt}'
  ]

  del(cleanPaths).then(function (paths) {
    callback()
  })
}

gulp.task('clean', cleanTask)
module.exports = cleanTask

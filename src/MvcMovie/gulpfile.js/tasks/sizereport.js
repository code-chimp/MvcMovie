var gulp = require('gulp')
var sizereport = require('gulp-sizereport')
var config = require('../config')

gulp.task('size-report', function () {
  return gulp.src([config.root.dest + '/**/*', '*!rev-manifest.json'])
    .pipe(sizereport({ gzip: true }))
})

var gulp = require('gulp')
var changed = require('gulp-changed')
var path = require('path')
var config = require('../config')

var paths = {
  src: [
    path.join(config.root.src, config.tasks.statics.src, '/**'),
    path.join('!' + config.root.src, config.tasks.statics.src, '/README.md')
  ],
  dest: path.join(config.root.dest, config.tasks.statics.dest)
}

var staticTask = function () {
  return gulp.src(paths.src)
    .pipe(changed(paths.dest)) // Ignore unchanged files
    .pipe(gulp.dest(paths.dest))
}

gulp.task('statics', staticTask)
module.exports = staticTask

// https://www.npmjs.com/package/gulp-standard

var gulp = require('gulp')
var standard = require('gulp-standard')
var path = require('path')
var handleErrors = require('../lib/handleErrors')
var config = require('../config')

var paths = {
  src: [
    path.join(config.root.src, config.tasks.js.src, '/**/*.js'),
    path.join(config.tasks.lint.gulpSrc, '/**/*.js')
  ]
}

var lintTask = function () {
  return gulp.src(paths.src)
    .pipe(standard())
    .on('error', handleErrors)
    .pipe(standard.reporter('default', {breakOnError: true}))
}

gulp.task('lint', lintTask)
module.exports = lintTask
